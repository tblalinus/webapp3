﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WhiskyDB.Domain;

namespace WhiskyDB.Data
{
    public class WhiskyDBContext : DbContext
    {

        public DbSet<Whisky> Whiskys { get; set; }
        public DbSet<Destillery> Destilleries { get; set; }
        public DbSet<Cask> Casks { get; set; }
        public DbSet<WhiskyCask> WhiskyCasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
              "Server=tcp:whiskydatabas.database.windows.net,1433;Initial Catalog=WhiskyDataBas;Persist Security Info=False;User ID=Linus;Password=Slavskepp4818;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
    }
}

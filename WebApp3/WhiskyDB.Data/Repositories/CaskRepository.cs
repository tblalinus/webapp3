﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhiskyDB.Data;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public class CaskRepository : ICaskRepository
    {
        private readonly WhiskyDBContext _context;

        public CaskRepository(WhiskyDBContext context)
        {
            _context = context;
        }

        public List<Cask> GetAll()
        {
                return _context.Casks.Include(c => c.WhiskyCasks).ToList();
        }

        public IQueryable<Cask> GetByWhisky(Whisky whisky)
        {
           var whiskyCasks = _context.WhiskyCasks.Where(wc => wc.WhiskyId == whisky.Id);
            var caskIds = whiskyCasks.Select(wc => wc.CaskId);
            return _context.Casks.Where(c => caskIds.Contains(c.Id));
        }

        public Cask GetById(int id)
        {
            return GetAll().SingleOrDefault(d => d.Id == id);
        }

        public void Add(Cask cask)
        {
                _context.Casks.Add(cask);
                _context.SaveChanges();
        }

        public void Delete(Cask cask)
        {
            _context.Remove(cask);
            _context.SaveChanges();
        }

        public void Edit(int id, string name, int? litreVolume, Cask.Predecessors predecessor)
        {
            Cask cask = GetById(id);
            cask.Name = name;
            cask.LitreVolume = litreVolume;
            cask.Predecessor = predecessor;
            _context.SaveChanges();
        }
    }
}

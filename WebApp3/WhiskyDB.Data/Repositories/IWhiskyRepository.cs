﻿using System;
using System.Collections.Generic;
using System.Text;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public interface IWhiskyRepository
    {
        List<Whisky> GetAll();
        List<Whisky> GetRandom(int i);
        Whisky GetById(int id);
        List<Whisky> GetByCask(Cask cask);
        void Add(Whisky whisky, List<Cask> cask);
        void Delete(Whisky whisky);
        void Edit(int id, string name, int? age, string description, string imageUrl, Destillery destillery, List<Cask> casks);
    }
}

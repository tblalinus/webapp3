﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhiskyDB.Data;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public class WhiskyRepository : IWhiskyRepository
    {
        private readonly WhiskyDBContext _context;

        public WhiskyRepository(WhiskyDBContext context)
        {
            _context = context;
        }

        public List<Whisky> GetAll()
        {
                return _context.Whiskys.Include(w => w.Destillery).ToList();
        }

        public List<Whisky> GetRandom(int i)
        {
            Random rnd = new Random();
            List<Whisky> whiskies = GetAll();
            int n = whiskies.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                Whisky whisky = whiskies[k];
                whiskies[k] = whiskies[n];
                whiskies[n] = whisky;
            }
            return whiskies.Take(i).ToList();
        }

        public Whisky GetById(int id)
        {
            return GetAll().SingleOrDefault(w => w.Id == id);
        }

        public List<Whisky> GetByCask(Cask cask)
        {
            var whiskyCasks = _context.WhiskyCasks.Where(wc => wc.CaskId == cask.Id);
            var whiskyIds = whiskyCasks.Select(wc => wc.WhiskyId);
            return GetAll().Where(w => whiskyIds.Contains(w.Id)).ToList();
        }

        public void Add(Whisky whisky, List<Cask> casks)
        {
            _context.Whiskys.Add(whisky);
            foreach(Cask cask in casks)
            {
                WhiskyCask whiskyCask = new WhiskyCask() { Whisky = whisky, Cask = cask };
                _context.WhiskyCasks.Add(whiskyCask);
            }
            _context.SaveChanges();
        }

        public void Delete(Whisky whisky)
        {
            _context.Whiskys.Remove(whisky);
            _context.SaveChanges();
        }

        public void Edit(int id, string name, int? age, string description, string imageUrl, Destillery destillery, List<Cask> casks)
        {
            Whisky whisky = GetById(id);
            whisky.Name = name;
            whisky.Age = age;
            whisky.Description = description;
            whisky.ImageUrl = imageUrl;
            whisky.Destillery = destillery;

            _context.WhiskyCasks.RemoveRange(_context.WhiskyCasks.Where(c => c.WhiskyId == id));
            foreach (Cask cask in casks)
            {
                WhiskyCask newWhiskyCask = new WhiskyCask { Whisky = whisky, Cask = cask };
                _context.WhiskyCasks.Add(newWhiskyCask);
            }

            _context.SaveChanges();
        }
    }
}

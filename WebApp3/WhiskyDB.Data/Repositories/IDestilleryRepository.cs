﻿using System;
using System.Collections.Generic;
using System.Text;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public interface IDestilleryRepository
    {
        List<Destillery> GetAll();
        Destillery GetById(int id);
        void Add(Destillery destillery);
        void Delete(Destillery destillery);
        void Edit(int id, string name, int? foundingYear, Destillery.Districts district);
    }
}

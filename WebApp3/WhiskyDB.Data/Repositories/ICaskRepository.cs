﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public interface ICaskRepository
    {
        List<Cask> GetAll();
        IQueryable<Cask> GetByWhisky(Whisky whisky);
        Cask GetById(int id);
        void Add(Cask cask);
        void Delete(Cask cask);
        void Edit(int id, string name, int? litreVolume, Cask.Predecessors predecessor);
    }
}

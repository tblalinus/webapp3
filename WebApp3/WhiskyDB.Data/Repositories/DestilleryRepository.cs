﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WhiskyDB.Data;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Repositories
{
    public class DestilleryRepository : IDestilleryRepository
    {
        private readonly WhiskyDBContext _context;

        public DestilleryRepository(WhiskyDBContext context)
        {
            _context = context;
        }

        public List<Destillery> GetAll()
        {
                return _context.Destilleries.Include(d => d.Whiskys).ToList();
        }

        public Destillery GetById(int id)
        {
            return GetAll().SingleOrDefault(c => c.Id == id);
        }

        public void Add(Destillery destillery)
        {
                _context.Destilleries.Add(destillery);
                _context.SaveChanges();
        }

        public void Delete(Destillery destillery)
        {
            _context.Remove(destillery);
            _context.SaveChanges();
        }

        public void Edit(int id, string name, int? foundingYear, Destillery.Districts district)
        {
            Destillery destillery = GetById(id);
            destillery.Name = name;
            destillery.FoundingYear = foundingYear;
            destillery.District = district;
            _context.SaveChanges();
        }
    }
}

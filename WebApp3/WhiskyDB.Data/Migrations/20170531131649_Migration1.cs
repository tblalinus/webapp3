﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WhiskyDB.Data.Migrations
{
    public partial class Migration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Casks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LitreVolume = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Predecessor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Casks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Destilleries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    District = table.Column<int>(nullable: false),
                    FoundingYear = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Destilleries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Whiskys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Age = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DestilleryId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Whiskys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Whiskys_Destilleries_DestilleryId",
                        column: x => x.DestilleryId,
                        principalTable: "Destilleries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WhiskyCasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CaskId = table.Column<int>(nullable: false),
                    WhiskyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WhiskyCasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WhiskyCasks_Casks_CaskId",
                        column: x => x.CaskId,
                        principalTable: "Casks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WhiskyCasks_Whiskys_WhiskyId",
                        column: x => x.WhiskyId,
                        principalTable: "Whiskys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Whiskys_DestilleryId",
                table: "Whiskys",
                column: "DestilleryId");

            migrationBuilder.CreateIndex(
                name: "IX_WhiskyCasks_CaskId",
                table: "WhiskyCasks",
                column: "CaskId");

            migrationBuilder.CreateIndex(
                name: "IX_WhiskyCasks_WhiskyId",
                table: "WhiskyCasks",
                column: "WhiskyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WhiskyCasks");

            migrationBuilder.DropTable(
                name: "Casks");

            migrationBuilder.DropTable(
                name: "Whiskys");

            migrationBuilder.DropTable(
                name: "Destilleries");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WhiskyDB.Data;
using WhiskyDB.Domain;

namespace WhiskyDB.Data.Migrations
{
    [DbContext(typeof(WhiskyDBContext))]
    [Migration("20170531131649_Migration1")]
    partial class Migration1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WhiskyDB.Domain.Cask", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LitreVolume");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Predecessor");

                    b.HasKey("Id");

                    b.ToTable("Casks");
                });

            modelBuilder.Entity("WhiskyDB.Domain.Destillery", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("District");

                    b.Property<int>("FoundingYear");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Destilleries");
                });

            modelBuilder.Entity("WhiskyDB.Domain.Whisky", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Age");

                    b.Property<string>("Description");

                    b.Property<int?>("DestilleryId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("DestilleryId");

                    b.ToTable("Whiskys");
                });

            modelBuilder.Entity("WhiskyDB.Domain.WhiskyCask", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CaskId");

                    b.Property<int>("WhiskyId");

                    b.HasKey("Id");

                    b.HasIndex("CaskId");

                    b.HasIndex("WhiskyId");

                    b.ToTable("WhiskyCasks");
                });

            modelBuilder.Entity("WhiskyDB.Domain.Whisky", b =>
                {
                    b.HasOne("WhiskyDB.Domain.Destillery", "Destillery")
                        .WithMany("Whiskys")
                        .HasForeignKey("DestilleryId");
                });

            modelBuilder.Entity("WhiskyDB.Domain.WhiskyCask", b =>
                {
                    b.HasOne("WhiskyDB.Domain.Cask", "Cask")
                        .WithMany("WhiskyCasks")
                        .HasForeignKey("CaskId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WhiskyDB.Domain.Whisky", "Whisky")
                        .WithMany("WhiskyCasks")
                        .HasForeignKey("WhiskyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WhiskyDB.Data.Repositories;
using WhiskyDB.Domain;
using WhiskyDB.App.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WhiskyDB.App.Controllers
{
    public class WhiskyController : Controller
    {
        private readonly IWhiskyRepository _whiskyRepository;
        private readonly IDestilleryRepository _destilleryRepository;
        private readonly ICaskRepository _caskRepository;

        public WhiskyController(IWhiskyRepository whiskyRepository, IDestilleryRepository destilleryRepository, ICaskRepository caskRepository)
        {
            _whiskyRepository = whiskyRepository;
            _destilleryRepository = destilleryRepository;
            _caskRepository = caskRepository;
        }

        public IActionResult Index()
        {
            return View(_whiskyRepository.GetAll());
        }

        public IActionResult Add()
        {
            ViewBag.Destilleries = _destilleryRepository.GetAll();
            ViewBag.Casks = _caskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public IActionResult Add(AddWhiskyViewModel model)
        {
            if (ModelState.IsValid)
            {
                Whisky whisky = model.Whisky;
                whisky.Destillery = _destilleryRepository.GetById(model.DestilleryId);
                List<Cask> casks = _caskRepository.GetAll().Where(c => model.CaskIds.Contains(c.Id)).ToList();
                _whiskyRepository.Add(model.Whisky, casks);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Destilleries = _destilleryRepository.GetAll();
                ViewBag.Casks = _caskRepository.GetAll();
                return View();
            }
        }

        public IActionResult Details(int id)
        {
            Whisky whisky = _whiskyRepository.GetById(id);
            IQueryable<Cask> casks = _caskRepository.GetByWhisky(whisky);
            return View(new WhiskyDetailsViewModel() { Whisky = whisky, Casks = casks });
        }

        public IActionResult Delete(int id)
        {
            Whisky whisky = _whiskyRepository.GetById(id);
            _whiskyRepository.Delete(whisky);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Whisky whisky = _whiskyRepository.GetById(id);
            List<int> caskIds = _caskRepository.GetByWhisky(whisky).Select(c => c.Id).ToList();
            ViewBag.Destilleries = _destilleryRepository.GetAll();
            ViewBag.Casks = _caskRepository.GetAll();
            return View(new AddWhiskyViewModel() { Whisky = whisky, DestilleryId = whisky.Destillery.Id, CaskIds = caskIds });
        }

        [HttpPost]
        public IActionResult Edit(AddWhiskyViewModel model)
        {
            if (ModelState.IsValid)
            {
                Whisky whisky = model.Whisky;
                whisky.Destillery = _destilleryRepository.GetById(model.DestilleryId);
                List<Cask> casks = _caskRepository.GetAll().Where(c => model.CaskIds.Contains(c.Id)).ToList();
                _whiskyRepository.Edit(model.Whisky.Id, model.Whisky.Name, model.Whisky.Age, model.Whisky.Description, model.Whisky.ImageUrl, model.Whisky.Destillery, casks);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Destilleries = _destilleryRepository.GetAll();
                ViewBag.Casks = _caskRepository.GetAll();
                return View(model);
            }
        }
    }
}
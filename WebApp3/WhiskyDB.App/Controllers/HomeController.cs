﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WhiskyDB.Domain;
using WhiskyDB.Data.Repositories;
using WhiskyDB.Data;

namespace WhiskyDB.App.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWhiskyRepository _repository;

        public HomeController(IWhiskyRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            List<Whisky> whiskies = _repository.GetRandom(3);
            return View(whiskies);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}

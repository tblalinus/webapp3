using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WhiskyDB.Domain;
using WhiskyDB.Data.Repositories;
using WhiskyDB.App.ViewModels;

namespace WhiskyDB.App.Controllers
{
    public class CaskController : Controller
    {
        private readonly ICaskRepository _caskRepository;
        private readonly IWhiskyRepository _whiskyRepository;

        public CaskController(ICaskRepository caskRepository, IWhiskyRepository whiskyRepository)
        {
            _caskRepository = caskRepository;
            _whiskyRepository = whiskyRepository;
        }

        public IActionResult Index()
        {
            return View(new CaskIndexViewModel() { Casks = _caskRepository.GetAll(), Whiskies = _whiskyRepository.GetAll() });
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Cask cask)
        {
            if (ModelState.IsValid)
            {
                _caskRepository.Add(cask);
                return RedirectToAction("Index");
            }
            else
                return View();
        }

        public IActionResult Delete(int id)
        {
            Cask cask = _caskRepository.GetById(id);
            List<Whisky> whiskies = _whiskyRepository.GetByCask(cask);
            if (whiskies.Count() == 0)
            {
                _caskRepository.Delete(cask);
            }
            else
                return View("CaskDeleteError");
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Cask cask = _caskRepository.GetById(id);
            return View(cask);
        }
        
        [HttpPost]
        public IActionResult Edit(Cask cask)
        {
            if (ModelState.IsValid)
            {
                _caskRepository.Edit(cask.Id, cask.Name, cask.LitreVolume, cask.Predecessor);
                return RedirectToAction("Index");
            }
            else
                return View();
        }
    }
}
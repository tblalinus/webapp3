using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WhiskyDB.Domain;
using WhiskyDB.Data.Repositories;

namespace WhiskyDB.App.Controllers
{
    public class DestilleryController : Controller
    {
        private readonly IDestilleryRepository _repository;

        public DestilleryController(IDestilleryRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            return View(_repository.GetAll());
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Destillery destillery)
        {
            if (ModelState.IsValid)
            {
                _repository.Add(destillery);
                return RedirectToAction("Index");
            }
            else
                return View();
        }

        public IActionResult Delete(int id)
        {
            Destillery destillery = _repository.GetById(id);
            if (destillery.Whiskys.Count == 0)
            {
                _repository.Delete(destillery);
            }
            else
                return View("DestilleryDeleteError");
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Destillery destillery = _repository.GetById(id);
            return View(destillery);
        }

        [HttpPost]
        public IActionResult Edit(Destillery destillery)
        {
            if (ModelState.IsValid)
            {
                _repository.Edit(destillery.Id, destillery.Name, destillery.FoundingYear, destillery.District);
                return RedirectToAction("Index");
            }
            else
                return View();
        }
    }
}
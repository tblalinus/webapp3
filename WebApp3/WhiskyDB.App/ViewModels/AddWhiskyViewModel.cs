﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WhiskyDB.Domain;

namespace WhiskyDB.App.ViewModels
{
    public class AddWhiskyViewModel
    {
        public Whisky Whisky { get; set; }
        [Required(ErrorMessage = "You must select at least one cask")]
        public IEnumerable<int> CaskIds { get; set; }
        public int DestilleryId { get; set; }
    }
}

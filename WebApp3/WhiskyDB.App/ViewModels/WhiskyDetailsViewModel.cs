﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhiskyDB.Domain;

namespace WhiskyDB.App.ViewModels
{
    public class WhiskyDetailsViewModel
    {
        public Whisky Whisky { get; set; }
        public IQueryable<Cask> Casks { get; set; }
    }
}

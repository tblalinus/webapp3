﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhiskyDB.Domain;

namespace WhiskyDB.App.ViewModels
{
    public class DestilleryIndexViewModel
    {
        public List<Destillery> Destilleries { get; set; }
        public List<Whisky> Whiskies { get; set; }
    }
}

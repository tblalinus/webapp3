﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WhiskyDB.Domain
{
    public class Destillery
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "You must enter a name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "You must enter a founding year")]
        [Range(1779, 2017, ErrorMessage = "Value must be between 1779 and 2017")]
        [Display (Name="Founded in")]
        public int? FoundingYear { get; set; }
        public enum Districts
        {
            [Display(Name = "Highland")]
            Highland,
            [Display(Name = "Lowland")]
            Lowland,
            [Display(Name = "SpeySide")]
            Speyside,
            [Display(Name = "Islay")]
            Islay,
            [Display(Name = "Islands")]
            Islands
        }
        [Required]
        public Districts District { get; set; }
        public List<Whisky> Whiskys { get; set; }
    }
}

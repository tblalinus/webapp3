﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WhiskyDB.Domain
{
    public class Whisky
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "You must enter a name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "You must enter an age")]
        [Range(3, 100, ErrorMessage = "Value must be between 3 and 100")]
        public int? Age { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "You must enter an imageUrl")]
        [DataType (DataType.ImageUrl)]
        public string ImageUrl { get; set; }
        public Destillery Destillery { get; set; }
        public List<WhiskyCask> WhiskyCasks { get; set; }
    }
}

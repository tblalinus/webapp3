﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WhiskyDB.Domain
{
    public class WhiskyCask
    {
        public int Id { get; set; }
        public Whisky Whisky { get; set; }
        public int WhiskyId { get; set; }
        public Cask Cask { get; set; }
        public int CaskId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WhiskyDB.Domain
{
    public class Cask
    {
        public int Id { get; set; }
        [Required (ErrorMessage = "You must enter a name")]
        public string Name { get; set; }
        [Required (ErrorMessage = "You must enter a volume")]
        [Range (50, 1000, ErrorMessage = "Value must be between 50 and 1000")]
        [Display (Name="Volume in litres")]
        public int? LitreVolume { get; set; }
        public enum Predecessors
        {
            [Display(Name = "None")]
            None,
            [Display(Name = "Bourbon")]
            Bourbon,
            [Display(Name = "Sherry")]
            Sherry,
            [Display(Name = "Wine")]
            Wine,
            [Display(Name = "Rum")]
            Rum,
            [Display(Name = "Port")]
            Port,
            [Display(Name = "Muscat")]
            Muscat
        }
        [Required]
        public Predecessors Predecessor { get; set; }
        public List<WhiskyCask> WhiskyCasks { get; set; }
    }
}
